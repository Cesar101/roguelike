﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public int hitPoints = 2;
	public Sprite damagedWallsSprite;

	private SpriteRenderer spriteRenderer;

	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	 public void DamageWall(int damageRecieved) 
	 {
		hitPoints -= damageRecieved;
		spriteRenderer.sprite = damagedWallsSprite;

		if (hitPoints <= 0) 
		{
			gameObject.SetActive(false);
		}
	}

}